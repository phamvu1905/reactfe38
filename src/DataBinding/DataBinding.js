import React, { Component } from "react";

class DataBinding extends Component {
  // Biến trong rcc ngoài phương thức  =>  thuộc tính => truy xuất thông qua this
  userName = "Khai fe38";
  hocVien = {
    maHV: 1,
    tenHV: "Nguyễn văn a",
    tuoi: 19,
  };
  render() {
    let { maHV, tenHV, tuoi } = this.hocVien;
    // Biến var let const
    const age = 20;

    return (
      <div>
        <p id="text">{this.userName}</p>
        <p id="text" tuoi>
          {age}
        </p>
        <ul>
          <li>mã: {maHV}</li>
          <li>tên: {tenHV}</li>
          <li>tuổi: {tuoi}</li>
        </ul>
      </div>
    );
  }
}

export default DataBinding;
