import React from "react";
import { Component } from "react";
import logo from "./logo.svg";
import "./App.css";

// import { student } from "./Demo/demo";
// import Demo from "./Demo/demo";
// import Demo2 from "./Demo/demo2";
// import Demo3 from "./Demo/demoFunction";

import Exercise2Index from "./exercise-2/Index";
import DataBinding from "./DataBinding/DataBinding";
import EventHandle from "./EventHandle/EventHandle";
class App extends Component {
  render() {
    return (
      <div>
        {/* <Exercise2Index /> */}
        {/* <DataBinding/> */}
        <EventHandle/>
      </div>
    );
  }
}

export default App;
