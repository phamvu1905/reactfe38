import React from "react";
import { Component } from "react";

class DemoComponent2 extends Component {
  render() {
    return (
      <div>
        <h1>Demo 2</h1>
        <p>This is Demo 2</p>
      </div>
    );
  }
}
export default DemoComponent2;
