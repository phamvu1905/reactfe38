//DEMO COMPONENT CLASS: chỉ là một class thông thường
import React from "react";
import { Component } from "react";

class DemoComponent extends Component {
  render() {
    return <h1>This is a demo Component</h1>;
  }
}

export default DemoComponent;

// export const student = "a";
