import React, { Component } from "react";
import Footer from "../Footer";
import Header from "../Header";
import Content from "../Content";
import Sliderbar from "../Slidebar";
import "./style.css";
class Index extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="container">
          <Content />
          <Sliderbar />
        </div>

        <Footer />
      </div>
    );
  }
}

export default Index;
