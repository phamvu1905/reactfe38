import React, { Component } from "react";

class EventHandle extends Component {
  // document.getElementById('id').addEventListener('click',function(){});
  // document.getElementById('id').onclick = function(){} ;
  showMessage = () => {
    alert("Hello frontend 38");
  };
  //   xử lí sự kiện với truyền tham số (params)
  showProfile = (profile) => {
    console.log(profile);
  };

  showProfileCallBack = (hocVien, _this) => {
      console.log(hocVien);
      console.log(_this);

  };

  render() {
    let hocVien = {
      maHV: 1,
      tenHV: "lê văn tèo",
      lop: "frontend 38",
    };
    return (
      <div>
        {/* Cách 1: xử lý sự kiện dùng function 1 lần */}
        <button
          onClick={() => {
            alert("hahaha");
          }}
        >
          click me!!!
        </button>
        {/* Cách 1: xử lý sự kiện dùng function nhiều lần */}
        <button onClick={this.showMessage}>click me!!!</button>
        {/* xử lý sự kiện với tham số (params) */}
        <button
          onClick={() => {
            this.showProfile(hocVien);
          }}
        >
          show profile
        </button>
        <button onClick={this.showProfileCallBack.bind(this,hocVien)}>show profile callback</button>
          
      </div>
    );
  }
}

export default EventHandle;
